
This project explore RAG techniques like query expansion and embedding adaptators 
It follow the following course from DeepLearning.AI https://www.deeplearning.ai/short-courses/advanced-retrieval-for-ai/  

## Requirements 
* python3.10
* install ollama from https://ollama.ai/ then run command "ollama run mistral"


# Install project dependencies 
* make install-package 

# Env Variables 
In your .env fill DATAPATH with your data directory

# Jupyter Notebook 
* kernel from pyenv created by poetry in path .venv/bin/python 
* jupyter/exploration.ipynb 